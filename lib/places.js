
ColPlaces = new Mongo.Collection("places");

ColPlaces.initEasySearch(["title", "description"], {
							"use": "mongo-db"
						});

if (Meteor.isClient)
{
	Meteor.subscribe("places");

	Template.registerHelper('equals',
    				function(v1, v2) {
        			   return (v1 === v2);
    				}
	);

	Template.Places.helpers ({
		places: function () {
			var allPlaces = ColPlaces.find({});
			return allPlaces;
		}
	});

	Template.Home.events({
		"click #gotop": function(event){
			$("html, body").animate({ scrollTop: 0 }, 600);
				return false;
		}
	});

	Template.sidebar.events({
		"click #sidebar": function(event){
			$('#search-bar').toggle('fast', function() {
	// Animation complete.
				});
		}
	});

}

if (Meteor.isServer)
{
	Meteor.publish("places", function () {
		var tousPlaces = ColPlaces.find({});
		return tousPlaces;
		});
}
